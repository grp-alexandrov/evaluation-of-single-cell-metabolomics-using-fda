# Evaluation of single-cell metabolomics using FDA

### Increasing quantitation in spatial single-cell metabolomics by using fluorescence as ground truth
Martijn R. Molenaar, Mohammed Shahraz, Jeany Delafiori, Andreas Eisenbarth, Måns Ekelöf, Luca Rappez, Theodore Alexandrov

The source code to reproduce the figures of the project can be found under the files:
 - Figures_in_manuscript.Rmd
 - Figures_in_manuscript_co_culture.Rmd

## raw data will be soon available via Metabolights (#MTBLS6465)
During the curation process, please contact us for the raw data files
